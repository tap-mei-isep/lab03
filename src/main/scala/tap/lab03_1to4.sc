//  #1
val aList:List[Int] = List(1, 2, 3)
val bList=List("edom", "odsoft", "tap")
val cList=List('a', 'b')
val dList=List(true, false)
val e=5.6
val fList = List(1.0, 2, 3)
val g='i'

//> a)

//> b)

//> c)

//> d)

//> e)


//  #2
//> a)

//> b)


//  #3

val l = List("Maria", "Ana", "Joana", "Julia", "Paulo", "José")

//> a)

//> b)


//  #4

val x = ???
List(1, 2, 3, -1, -2, -3, 0).map(x)
//> Result: List[Int] = List(1, 2, 3, 1, 2, 3, 0)
